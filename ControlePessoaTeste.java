import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import controle.ControlePessoa;
import controle.Pessoa;


public class ControlePessoaTeste {

	private ControlePessoa umControle;
	
	@Before
	public void setUp() throws Exception{
		
	}
	@Test
	public void testAdicionar() {
		umControle = new ControlePessoa();
		Pessoa pessoaTest = new Pessoa();
		pessoaTest.setNome("Rafael");
		umControle.adicionar(pessoaTest);
		assertEquals(umControle.adicionar(pessoaTest), "Pessoa adicionada com Sucesso!");
	}
	
	@Test
	public void testPesquisar() {
		umControle = new ControlePessoa();
		Pessoa pessoaTest = new Pessoa();
		pessoaTest.setNome("Rafael");
		umControle.adicionar(pessoaTest);
		umControle.pesquisar("Rafael");
		assertEquals(umControle.pesquisar("Rafael"), "Rafael");
	}
	
	@Test
	public void testRemover() {
		umControle = new ControlePessoa();
		Pessoa pessoaTest = new Pessoa();
		pessoaTest.setNome("Rafael");
		umControle.adicionar(pessoaTest);
		umControle.remover(pessoaTest);
		assertEquals(umControle.remover(pessoaTest), "Pessoa removida com sucesso!");
	}
}
