import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import controle.Pessoa;
import controle.Pessoa;


public class PessoaTeste {

	private Pessoa pessoaTeste;
		
	@Before
	public void setUp() throws Exception{
		
	}
	@Test
	public void testNome() {
		pessoaTeste = new Pessoa();
		pessoaTeste.setNome("Paulo");
		
		assertEquals(pessoaTeste.getNome(), "Paulo");
	}
	
	@Test
	public void testTelefone() {
		pessoaTeste = new Pessoa();
		pessoaTeste.setTelefone("555-555");
		
		assertEquals(pessoaTeste.getTelefone(), "555-555");
	}
	
	@Test
	public void testEmail() {
		pessoaTeste = new Pessoa();
		pessoaTeste.setEmail("a@a.com");
		
		assertEquals(pessoaTeste.getEmail(), "a@a.com");
	}
	
	@Test
	public void testIdade() {
		pessoaTeste = new Pessoa();
		pessoaTeste.setIdade("18");
		
		assertEquals(pessoaTeste.getIdade(), "18");
	}
	
	@Test
	public void testcpf() {
		pessoaTeste = new Pessoa();
		pessoaTeste.setCpf("12345");
		
		assertEquals(pessoaTeste.getCpf(), "12345");
	}
	
	@Test
	public void testRg() {
		pessoaTeste = new Pessoa();
		pessoaTeste.setRg("54321");
		
		assertEquals(pessoaTeste.getRg(), "54321");
	}

}
